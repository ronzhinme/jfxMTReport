/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jfxMTReportApp;

import GeneralObjects.RepFileOperation;
import GeneralObjects.ReportPage;
import GeneralObjects.ReportRect;
import PropertyGrid.*;
import com.sun.javafx.print.Units;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javax.imageio.ImageIO;

/**
 *
 * @author 1
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button btnAdd;
    @FXML
    private Button btnDel;
    @FXML
    private TreeView<?> treeview;
    @FXML
    private TableView<?> gridview;
    @FXML
    private Canvas canvas;
    @FXML
    private GridPane tableview;

    private Image imgAdd_;
    private Image imgDel_;
    private final String delimgbtn_file_ = "DelBtn.bmp";
    private final String addimgbtn_file_ = "AddBtn.bmp";


            
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imgAdd_ = new Image(getClass().getResourceAsStream(addimgbtn_file_));
        imgDel_ = new Image(getClass().getResourceAsStream(delimgbtn_file_));
        btnAdd.setGraphic(new ImageView(imgAdd_));
        btnDel.setGraphic(new ImageView(imgDel_));
        
        TreeItem item = new TreeItem("+");
        treeview.setRoot(item);
    }

    private void TestCanvasDraw() throws IOException {
        ReportPage pg = new ReportPage(canvas);
        ReportRect rect = new ReportRect(1, 2, 3, 4, Units.INCH);
        pg.Draw();
        rect.Draw(pg.getGC());
    }

   
            
    private void AddNewElement()
    {
        TreeItem ti = new TreeItem("New Item");
        treeview.getRoot().getChildren().add(ti);
    }
    
    @FXML
    private void btnAddClick() throws IOException {
        AddNewElement();
    }

    @FXML
    private void btnDelClick() {

        PropertyGrid.PropertyText txtprop = new PropertyText();
        txtprop.SetName("labelus_propertus");
        txtprop.SetValue("valus_propertus");
        PropertyTableBuilder.AddProperty(tableview, txtprop);

        PropertyGrid.PropertyInteger intprop = new PropertyInteger();
        intprop.SetName("labelus_integerus");
        intprop.SetValue("");
        PropertyTableBuilder.AddProperty(tableview, intprop);
    }

    @FXML
    private void menuActionExit() {
        Platform.exit();
    }

    @FXML
    private void menuActionOpen() throws IOException {
        RepFileOperation.LoadProject();
    }

    @FXML
    private void menuActionSave() throws IOException {
        RepFileOperation.SaveProject();
    }

    @FXML
    private void menuActionExport() throws IOException {
        RepFileOperation.SaveToPng(canvas);
    }
}
