/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralObjects;

import com.sun.javafx.print.Units;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author rongin
 */
public final class ReportRect implements RepObj, SizeBasedObject {

    private double height_;
    private double width_;
    private double xpos_;
    private double ypos_;
    private Units units_;

    public ReportRect() {
        height_ = 10;
        width_ = 10;
        xpos_ = 0;
        ypos_ = 0;
        units_ = Units.POINT;
    }

    public ReportRect(double posx, double posy, double w, double h, Units u) {
        double koef = UnitConvertor.getKoef( u, Units.POINT);
        units_ = u;
        setSize(w*koef, h*koef);
        xpos_ = posx*koef;
        ypos_ = posy*koef;
    }
    
    @Override
    public void Draw(GraphicsContext gc) {
        gc.fillRect(xpos_, ypos_, width_, height_);
    }

    @Override
    public void setSize(double x, double y) {
        height_ = x;
        width_ = y;
    }

    @Override
    public double getWidth() {
        return width_;
    }

    @Override
    public double getHeight() {
        return height_;
    }
}
