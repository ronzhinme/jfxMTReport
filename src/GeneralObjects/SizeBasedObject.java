/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralObjects;

/**
 *
 * @author rongin
 */
public interface SizeBasedObject {
    public void setSize(double x, double y);
    public double getWidth();
    public double getHeight();
}
