/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PropertyGrid;

import com.sun.org.apache.xpath.internal.operations.UnaryOperation;
import java.util.function.UnaryOperator;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author 1
 */
public class PropertyInteger extends PropertyBase {
    private final TextField txtval_;
    private final UnaryOperator<TextFormatter.Change> unaryOperator_ = (TextFormatter.Change t) -> {
        if (t.isAdded()) {
            IntegerStringConverter intconv = new IntegerStringConverter();
            int val = intconv.fromString(t.getText());
            t.setText(intconv.toString(val));
            t.setCaretPosition(t.getControlNewText().length());
            t.setAnchor(t.getControlNewText().length());
        }
        return t;
    };
    private final TextFormatter formatter_ = new TextFormatter(unaryOperator_);

    public PropertyInteger() {
        txtval_ = new TextField();
        txtval_.setTextFormatter(formatter_);
    }

    public void SetValue(String val) {
        txtval_.setText(val);
    }

    public String GetValue() {
        return txtval_.getText();
    }

    @Override
    public Node GetControl() {
        return txtval_;
    }
}
