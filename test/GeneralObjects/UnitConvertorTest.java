/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralObjects;

import com.sun.javafx.print.Units;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rongin
 */
public class UnitConvertorTest {
    
    public UnitConvertorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testToPoint() {
        double value = 1;
        Units fromunit = Units.INCH;
        double expResult = 72;
        double result = UnitConvertor.toPoint(value, fromunit);
        assertEquals(expResult, result, 0.0);
        
        value = 1;
        fromunit = Units.MM;
        expResult = 72/25.4;
        result = UnitConvertor.toPoint(value, fromunit);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testToInch() {
        double value = 1;
        Units fromunit = Units.POINT;
        double expResult = 1/72.0;
        double result = UnitConvertor.toInch(value, fromunit);
        assertEquals(expResult, result, 0.0);       
        
        value = 1;
        fromunit = Units.MM;
        expResult = 1/25.4;
        result = UnitConvertor.toInch(value, fromunit);
        assertEquals(expResult, result, 0.0);       
    }

    @Test
    public void testToMM() {
        double value = 1;
        Units fromunit = Units.POINT;
        double expResult = 25.4/72.0;
        double result = UnitConvertor.toMM(value, fromunit);
        assertEquals(expResult, result, 0.0);
        
        value = 1;
        fromunit = Units.INCH;
        expResult = 25.4;
        result = UnitConvertor.toMM(value, fromunit);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetKoef() {
        Units from = Units.MM;
        Units to = Units.POINT;
        double expResult = 72/25.4;
        double result = UnitConvertor.getKoef(from, to);
        assertEquals(expResult, result, 0.0);
        
        from = Units.INCH;
        to = Units.POINT;
        expResult = 72;
        result = UnitConvertor.getKoef(from, to);
        assertEquals(expResult, result, 0.0);
    }
    
}
